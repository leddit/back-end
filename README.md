# Projeto Leddit

projeto em desenvolvimento para aprendizado.<br>
Repositorio <a href="https://www.gitlab.com/leddit/front-end" target="_blank">Front-end</a><br>

# TODO

- [ ] Banco de dados
  - [ ] Estruturar os dados
- [ ] CRUD de usuário
  - [ ] Cadastro de Usuário
  - [ ] Perfil do Usuário
  - [ ] Atualizar dados do Usuário
  - [ ] Apagar Usuário
- [ ] CRUD de posts
  - [ ] Fazer posts
  - [ ] Ver posts
  - [ ] Atualizar posts
  - [ ] Apagar posts
- [ ] Ações do Usuário
  - [ ] Seguir outros Usuários
  - [ ] Comentar comentar em posts
  - [ ] Mandar mensagens para outros Usuários

## License

Leddit is [MIT licensed](LICENSE).
