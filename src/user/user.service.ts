import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common'
import { CreateUserDto } from './dto/create-user.dto'
import { UpdateUserDto } from './dto/update-user.dto'
import { UpdateUserPasswordDto } from './dto/update-user-password.dto'
import { PrismaService } from '../prisma/prisma.service'
import { bcrypt } from '../utils/bcrypt'

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) {}

  async create(createUserDto: CreateUserDto) {
    const { name, email, password, confirmPassword } = createUserDto
    if (password === confirmPassword) {
      throw new BadRequestException("passwords don't match")
    }

    const encryptedPassword = await bcrypt.hash(password, 10)

    return this.prisma.user.create({
      data: {
        name,
        email,
        password: encryptedPassword
      }
    })
  }

  async findAll() {
    return this.prisma.user.findMany()
  }

  findOne(findUserDto: { id?: string; name?: string; email?: string }) {
    const { id, name, email } = findUserDto
    return this.prisma.user.findFirst({
      where: {
        id,
        name,
        email
      }
    })
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    const { name, email } = updateUserDto
    return this.prisma.user.update({
      where: {
        id
      },
      data: {
        name,
        email
      }
    })
  }

  async updatePassword(id: string, updateUserPasswordDto: UpdateUserPasswordDto) {
    const { currentPassword, newPassword, confirmNewPassword } = updateUserPasswordDto
    const user = await this.prisma.user.findFirst({
      where: {
        id
      }
    })
    if (!user) {
      throw new BadRequestException("user doesn't exist")
    }

    const isMatch = await bcrypt.compare(currentPassword, user.password)
    if (!isMatch) {
      throw new UnauthorizedException('incorrect password')
    }

    if (newPassword !== confirmNewPassword) {
      throw new BadRequestException("passwords don't match")
    }

    return this.prisma.user.update({
      where: {
        id
      },
      data: {
        password: newPassword
      }
    })
  }

  async remove(id: string) {
    return this.prisma.user.delete({
      where: {
        id
      }
    })
  }
}
