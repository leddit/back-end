import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common'
import { bcrypt } from '../utils/bcrypt'
import { UserService } from '../user/user.service'
import { JwtService } from '@nestjs/jwt'

@Injectable()
export class AuthService {
  constructor(private usersService: UserService, private jwtService: JwtService) {}

  async signIn(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne({ name: username })

    if (!user) {
      throw new BadRequestException()
    }
    const isMatch = bcrypt.compare(pass, user.password)
    if (!isMatch) {
      throw new UnauthorizedException()
    }

    const { id, name, email, verified } = user
    const payLoad = {
      id,
      name,
      email,
      verified
    }
    return {
      access_token: await this.jwtService.signAsync(payLoad)
    }
  }
}
