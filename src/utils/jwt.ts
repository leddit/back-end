export const jwtConfig = {
  JWT_SECRET: process.env.JWT_SECRET || '',
  EXPIRE_TIME: process.env.EXPIRE_TIME || '3m'
}
